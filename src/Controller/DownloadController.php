<?php

namespace Drupal\sharepoint_file_download\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sharepoint_api\SharepointClient;
use Microsoft\Graph\Model\DriveItem;
use Microsoft\Graph\Model\File;
use Microsoft\Graph\Model\Hashes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Sharepoint file download routes.
 */
class DownloadController extends ControllerBase {

  /**
   * The sharepoint client.
   *
   * @var \Drupal\sharepoint_api\SharepointClient
   */
  private SharepointClient $sharepointClient;

  /**
   * The constructor.
   */
  public function __construct(SharepointClient $sharepoint_client) {
    $this->sharepointClient = $sharepoint_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sharepoint_api.client')
    );
  }

  /**
   * Builds the response.
   */
  public function build($file_id) {
    $file_item = $this->sharepointClient->getDriveItemByShareId($file_id);
    if (!$file_item instanceof DriveItem) {
      throw new NotFoundHttpException();
    }
    $file = $file_item->getFile();
    if (!$file instanceof File) {
      throw new NotFoundHttpException();
    }
    $hash = $file->getHashes();
    if (!$hash instanceof Hashes) {
      throw new NotFoundHttpException();
    }
    $properties = $hash->getProperties();
    // Probably this will not change, but I guess this will in theory make it
    // possible to request the same file several times. If a new hash is
    // computed of the same content. But it seems super weird if that was the
    // case. Also we can live with it, totally.
    $keys = array_keys($properties);
    $algo = reset($keys);
    $temp_name = $this->createTempName($file_id, $algo, $properties[$algo]);
    $this->downloadFile($file_id, $temp_name);
    $response = new BinaryFileResponse($temp_name);
    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $file_item->getName());
    $response->headers->set('Content-Type', $file->getMimeType());
    return $response;
  }

  /**
   * Download the file.
   */
  protected function downloadFile($file_id, $temp_name) {
    $this->sharepointClient->downloadFileByShareId($file_id, $temp_name);
  }

  /**
   * Create the temp name.
   */
  protected function createTempName($file_id, $algo, $hash) {
    return sprintf('/tmp/sharepoint-download-%s-%s-%s', sha1($file_id), sha1($algo), sha1($hash));
  }

}
